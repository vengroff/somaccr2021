
from collections import defaultdict
from matplotlib import patheffects
import numpy as np


class SquareAnimation:

    class Dot:
        def __init__(self, color, x, y, square_idx, dot_size=10):
            self._color = color
            self._square_idx = square_idx
            self._dot_size = dot_size
            self._pos = (x, y)

        def set_pos(self, x, y):
            self._pos = (x, y)

        @property
        def color(self):
            return self._color

        @property
        def square_idx(self):
            return self._square_idx

        @square_idx.setter
        def square_idx(self, value):
            self._square_idx = value

        @property
        def x(self):
            return self._pos[0]

        @property
        def y(self):
            return self._pos[1]

    def __init__(
        self,
        sub_squares=3,
        sub_square_size=9,
        dot_size=81,
        dot_spacing=20,
        title=None,
        text=None,
    ):
        self._sub_squares = sub_squares
        self._sub_square_size = sub_square_size

        self._dot_size = dot_size
        self._dot_spacing = dot_spacing

        self._n = sub_squares * sub_squares * sub_square_size * sub_square_size

        # First level key is tag. Second is color.
        self._dots = defaultdict(lambda: defaultdict(list))
        self._dot_destinations = defaultdict(lambda: defaultdict(list))
        self._intermediates = defaultdict(lambda: defaultdict(list))
        self._square_color_count = defaultdict(lambda: defaultdict(int))

        self._bounds = []

        self._title = title
        self._text = text

        self._title_fontsize = 36

        self._square_titles = {}

        self.alpha = 1.0
        self._tag_alpha = defaultdict(lambda: 1.0)
        self.dot_alpha = 1.0
        self.boundary_alpha = 1.0
        self.title_alpha = 1.0
        self.text_alpha = 1.0
        self.square_title_alpha = 1.0

    def set_tag_alpha(self, tag, alpha):
        self._tag_alpha[tag] = alpha

    def tag_alpha(self, tag):
        return self._tag_alpha[tag]

    def dot_tags(self):
        return list(self._dots.keys())

    def num_sub_squares(self):
        return self._sub_squares * self._sub_squares

    def num_dots_per_sub_square(self):
        return self._sub_square_size * self._sub_square_size

    def _dot_xy(self, square_idx, dot_idx):
        square_x = square_idx % self._sub_squares
        square_y = square_idx // self._sub_squares

        dot_x = dot_idx % self._sub_square_size
        dot_y = dot_idx // self._sub_square_size

        x = (square_x * (self._sub_square_size + 1) + dot_x + 1.5) * self._dot_spacing
        y = (square_y * (self._sub_square_size + 1) + dot_y + 1.5) * self._dot_spacing

        return x, y

    def add_dot(self, color, square_idx, dot_idx, tag=""):
        x, y = self._dot_xy(square_idx, dot_idx)
        dot = SquareAnimation.Dot(color, x, y, square_idx, self._dot_size)
        self._dots[tag][color].append(dot)

        self._square_color_count[square_idx][color] = self._square_color_count[square_idx][color] + 1

    def pop_dot(self, color, tag):
        dot = self._dots[tag][color].pop()
        self._square_color_count[dot.square_idx][color] = self._square_color_count[dot.square_idx][color] - 1
        return dot

    def add_dot_destination(self, color, square_idx, dot_idx, tag=""):
        x, y = self._dot_xy(square_idx, dot_idx)
        dot = SquareAnimation.Dot(color, x, y, square_idx, self._dot_size)
        self._dot_destinations[tag][color].append(dot)

    def center_point(self):
        x0, y0 = self._dot_xy(0, 0)
        x1, y1 = self._dot_xy(self.num_sub_squares() - 1, self.num_dots_per_sub_square() - 1)

        return (x0 + x1) / 2.0, (y0 + y1) / 2.0

    def extent(self):
        x0, y0 = self._dot_xy(0, 0)
        x1, y1 = self._dot_xy(self.num_sub_squares() - 1, self.num_dots_per_sub_square() - 1)

        return x1 - x0, y1 - y0

    def random_points(self, n):
        cx, cy = self.center_point()
        ex, ey = self.extent()

        return [(np.random.normal(cx, ex / 4), np.random.normal(cy, ey / 4)) for _ in range(n)]

    def generate_intermediates(self):
        for tag, dots_by_color in self._dot_destinations.items():
            for color, dots in dots_by_color.items():
                self._intermediates[tag][color] = self.random_points(len(dots))

    def clear_intermediates(self):
        self._intermediates = defaultdict(lambda: defaultdict(list))

    def update_dots_to_destination(self):
        for tag in self._dot_destinations.keys():
            self._dots[tag] = self._dot_destinations[tag]
        self._dot_destinations = defaultdict(lambda: defaultdict(list))

        # Count them all up by squares.
        self._square_color_count = defaultdict(lambda: defaultdict(int))

        for tag, dots_by_color in self._dots.items():
            for color, dots in dots_by_color.items():
                for dot in dots:
                    self._square_color_count[dot.square_idx][color] = \
                        self._square_color_count[dot.square_idx][color] + 1

    def population(self, squares_indices=None):
        if squares_indices is None:
            squares_indices = range(self._sub_squares * self._sub_squares)

        total_dots = 0

        for square_idx in squares_indices:
            for color, count in self._square_color_count[square_idx].items():
                total_dots = total_dots + count

        return total_dots

    def diversity(self, squares_indices=None):
        if squares_indices is None:
            squares_indices = range(self._sub_squares * self._sub_squares)

        total_dots = 0
        color_dots = defaultdict(int)

        for square_idx in squares_indices:
            for color, count in self._square_color_count[square_idx].items():
                total_dots = total_dots + count
                color_dots[color] = color_dots[color] + count

        weighted_sum = 0

        for color, count in color_dots.items():
            weighted_sum = weighted_sum + count * (total_dots - count)

        if total_dots != 0:
            diversity = 1.0 * weighted_sum / (total_dots * total_dots)
        else:
            diversity = 0.0

        return diversity

    def add_neigborhood_bounds(self, points):
        x = np.array([point[0] for point in points])
        y = np.array([point[1] for point in points])

        x = x * ((self._sub_square_size + 1) * self._dot_spacing) + (self._dot_spacing / 2)
        y = y * ((self._sub_square_size + 1) * self._dot_spacing) + (self._dot_spacing / 2)

        self._bounds.append({'x': x, 'y': y})

    def _configure_ax(self, ax, axis_off=True):
        ax.set_aspect('equal')

        cx, cy = self.center_point()
        ex, ey = self.extent()

        ax.set_xlim(
            cx - ex / 2 - 2 * self._dot_spacing,
            cx + ex / 2 + 2 * self._dot_spacing
        )
        ax.set_ylim(
            cy - ey / 2 - 2 * self._dot_spacing,
            cy + ey / 2 + 2 * self._dot_spacing
        )

        if axis_off:
            ax.set_axis_off()
        else:
            ax.set_axis_on()

    def _plot_bounds(self, ax):
        for bound in self._bounds:
            ax.plot(bound['x'], bound['y'], '-', color='grey', linewidth=3, alpha=self.alpha * self.boundary_alpha)

    def set_block_title(self, square_idx, title):
        self._square_titles[square_idx] = title

    def clear_block_title(self, square_idx):
        self._square_titles.pop(square_idx, None)

    def _plot_block_title(self, ax, square_idx, title):
        square_x = square_idx % self._sub_squares
        square_y = square_idx // self._sub_squares

        x = (square_x * (self._sub_square_size + 1) + 0.5 * self._sub_square_size + 1) * self._dot_spacing
        y = (square_y * (self._sub_square_size + 1) + 0.5 * self._sub_square_size + 1) * self._dot_spacing

        axtext = ax.text(x, y, title, fontsize=36,
                         color='black',
                         va='center', ha='center',
                         alpha=self.square_title_alpha * self.alpha)

        axtext.set_path_effects([patheffects.withStroke(linewidth=10, foreground="w")])

    def _plot_block_titles(self, ax):
        for square_idx, title in self._square_titles.items():
            self._plot_block_title(ax, square_idx, title)

    def plot(self, ax, plot_tags=None, exclude_tags=None, axis_off=True):
        if exclude_tags is None:
            exclude_tags = []
        if plot_tags is None:
            plot_tags = [key for key in self._dots.keys() if key not in exclude_tags]

        self._plot_bounds(ax)

        for tag in plot_tags:
            dots_by_color = self._dots[tag]
            for color, dots in dots_by_color.items():
                x = list([dot.x for dot in dots])
                y = list([dot.y for dot in dots])

                ax.scatter(x, y, color=color, s=self._dot_size,
                           alpha=self.alpha * self._tag_alpha[tag] * self.dot_alpha)

        self._plot_block_titles(ax)

        if self._title is not None:
            ax.text(12.5 / 16.0, 0.9, self._title,
                    transform=ax.transAxes,
                    fontsize=self._title_fontsize,
                    va='top', ha='center',
                    )

        self._configure_ax(ax, axis_off=axis_off)

    def plot_moving(self, ax, t, plot_tags=None, axis_off=True):
        if plot_tags is None:
            plot_tags = [key for key in self._dot_destinations.keys()]

        self._plot_bounds(ax)

        for tag in plot_tags:
            destinations_by_color = self._dot_destinations[tag]
            for color, destinations in destinations_by_color.items():
                dots = self._dots[tag][color]

                if len(dots) != len(destinations):
                    raise ValueError("Color {:} has {:} dots and {:} destinations.".format(
                        color, len(dots), len(destinations)
                    ))

                x0 = np.array([dot.x for dot in dots])
                y0 = np.array([dot.y for dot in dots])

                x1 = np.array([dot.x for dot in destinations])
                y1 = np.array([dot.y for dot in destinations])

                if tag in self._intermediates.keys():
                    intermediates = self._intermediates[tag][color]

                    if len(dots) != len(intermediates):
                        raise ValueError("Color {:} has {:} dots and {:} intermediates.".format(
                            color, len(dots), len(intermediates)
                        ))

                    xi = np.array([intermediate[0] for intermediate in intermediates])
                    yi = np.array([intermediate[1] for intermediate in intermediates])

                    x = (1 - t) * (1 - t) * x0 + 2.0 * t * (1 - t) * xi + t * t * x1
                    y = (1 - t) * (1 - t) * y0 + 2.0 * t * (1 - t) * yi + t * t * y1
                else:
                    x = (1 - t) * x0 + t * x1
                    y = (1 - t) * y0 + t * y1

                ax.scatter(x, y, color=color, s=self._dot_size,
                           alpha=self.alpha * self._tag_alpha[tag] * self.dot_alpha)

        self._plot_block_titles(ax)

        self._configure_ax(ax, axis_off=axis_off)

    # def plot_entering_exiting(self):
