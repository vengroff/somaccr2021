import os
import pandas as pd

USDI_ROOT = './drive/MyDrive/SOMA CCR/usdi'
DATA_ROOT = os.path.join(USDI_ROOT, 'data')


def _read_usdi(
        filename,
        str_fields,
        float_fields=None,
        details=False
):
    if float_fields is None:
        float_fields = ['diversity', 'integration', 'segregation']

    if details:
        kwargs = {}
    else:
        kwargs = {'usecols': str_fields + float_fields}

    df = pd.read_csv(
        os.path.join(DATA_ROOT, filename),
        dtype={**{field: str for field in str_fields}, **{field: float for field in float_fields}},
        **kwargs
    )

    return df


def read_usdi_states(year):
    df = _read_usdi(str(year) + '_us_state_diversity_integration.csv', ['STATEFP', 'NAME'])
    return df


def read_usdi_cousubs(year, details=False):
    df = _read_usdi(
        str(year) + '_us_cousub_diversity_integration.csv',
        [
            'GEOID', 'STATEFP', 'COUNTYFP', 'COUSUBFP', 'NAME'
        ],
        details=details,
    )

    return df


def read_usdi_counties(year):
    df = _read_usdi(
        str(year) + '_us_county_diversity_integration.csv',
        [
            'STATEFP', 'COUNTYFP',
        ],
        details=False,
    )

    return df


def read_usdi_block_groups(year, details=False):
    df = _read_usdi(
        str(year) + '_us_block_group_diversity_integration.csv',
        [
            'GEOID', 'STATEFP', 'COUNTYFP', 'TRACTFP', 'BLOCKGROUPFP',
        ],
        float_fields=['diversity'],
        details=details,
    )

    return df
