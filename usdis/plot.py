import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import statsmodels.api as sm

from matplotlib import patheffects

from statsmodels.sandbox.regression.predstd import wls_prediction_std

from usdis.census import FIELD_TOTAL_POPULATION
from somaccr2021.states import STATE_NAMES_FROM_IDS


class DollarFormatter(ticker.Formatter):

    def __init__(self, after_dec=0):
        self._num_format = ",.{:d}f".format(after_dec)

    def __call__(self, x, pos=None):
        """
        Return the formatted label string.

        *x* and *pos* are passed to `str.format` as keyword arguments
        with those exact names.
        """
        if x >= 0:
            return ("${x:" + self._num_format + "}K").format(x=x/1000, pos=pos)
        else:
            return ("(${x:" + self._num_format + "}K)").format(x=-x/1000, pos=pos)


def _plot_highlights(df_highlight, x_field, y_field, ax):
    # Temporary hack to avoid plotting Providence, VA when
    # we mean Providence, RI. Replace with better state spec for matching.
    #
    # from usdis.states import STATE_VA
    # df_highlight = df_highlight[df_highlight.STATEFP != STATE_VA]

    df_highlight.plot(x_field, y_field, kind='scatter',
                      color='black', marker='D', ax=ax,
                      s=288, zorder=100)
    df_highlight.plot(x_field, y_field, kind='scatter',
                      color='gold', marker='D', ax=ax,
                      s=200, zorder=100)
    for idx, row in df_highlight.iterrows():
        ax.annotate(
            s=row['NAME'] +', ' + STATE_NAMES_FROM_IDS[row['STATEFP']],
            xy=(row[x_field], row[y_field]),
            xytext=(20, 0),
            textcoords='offset points',
            fontsize=12,
            horizontalalignment='left',
            verticalalignment='center',
            color='black',
            path_effects=[patheffects.withStroke(linewidth=5, foreground="#ffffffdf")]
        )


def _scatter_plot(
        df_census,
        x_field,
        y_field,
        title,
        labels,
        plot_ols = True,
        plot_wls = False,
        plot_log_ols = False,
        size_population = False,
        regression_intercept = False,
        highlight_names = None,
        y_axis_label = None
):
    if size_population:
        df_census['plot_s'] = np.sqrt(df_census[FIELD_TOTAL_POPULATION])

    df_census_sorted = df_census.sort_values(by=x_field)

    if regression_intercept:
        df_census_sorted = sm.add_constant(df_census_sorted)
        df_x = df_census_sorted[[x_field, 'const']]
    else:
        df_x = df_census_sorted[[x_field]]

    df_y = df_census_sorted[y_field]

    if plot_ols:
        mod_ols = sm.OLS(
            df_y, df_x,
            hasconst=regression_intercept
        )
        res_ols = mod_ols.fit()
        prstd_ols, iv_l_ols, iv_u_ols = wls_prediction_std(res_ols)

    if plot_log_ols:
        df_log_y = np.log(df_y)

        mod_log_ols = sm.OLS(
            df_log_y, df_x,
            hasconst=regression_intercept
        )
        res_log_ols = mod_log_ols.fit()
        prstd_log_ols, iv_l_log_ols, iv_u_log_ols = wls_prediction_std(res_log_ols)

        iv_l_log_ols = np.exp(iv_l_log_ols)
        iv_u_log_ols = np.exp(iv_u_log_ols)

    if plot_wls:
        mod_wls = sm.WLS(
            df_y, df_x,
            weights=df_census[FIELD_TOTAL_POPULATION], hasconst=regression_intercept
        )
        res_wls = mod_wls.fit()
        prstd_wls, iv_l_wls, iv_u_wls = wls_prediction_std(res_wls)

    fig, ax = plt.subplots()

    ax = df_census.plot(
        x=x_field, y=y_field,
        s=('plot_s' if size_population else None),
        kind='scatter',
        ax=ax,
        label="Communities",
    )

    if plot_ols:
        fittedvalues = res_ols.fittedvalues
        ax.plot(df_census_sorted[x_field], fittedvalues, 'r-', label="OLS")
        ax.plot(df_census_sorted[x_field], iv_u_ols, 'r--')
        ax.plot(df_census_sorted[x_field], iv_l_ols, 'r--')

    if plot_log_ols:
        fittedvalues = res_log_ols.fittedvalues
        fittedvalues = np.exp(fittedvalues)
        ax.plot(df_census_sorted[x_field], fittedvalues, 'b-', label="OLS log(y)")
        ax.plot(df_census_sorted[x_field], iv_u_log_ols, 'b--')
        ax.plot(df_census_sorted[x_field], iv_l_log_ols, 'b--')

    if plot_wls:
        fittedvalues = res_wls.fittedvalues
        ax.plot(df_census_sorted[x_field], fittedvalues, 'g-', label="WLS")
        ax.plot(df_census_sorted[x_field], iv_u_wls, 'g--')
        ax.plot(df_census_sorted[x_field], iv_l_wls, 'g--')

    if highlight_names is not None:
        df_highlight = df_census[df_census.NAME.isin(highlight_names)]
        _plot_highlights(df_highlight, x_field, y_field, ax)

    ax.set_xlabel(x_field.capitalize(), fontsize=14)

    if y_axis_label is None:
        y_axis_label = " - ".join(labels)

    ax.set_ylabel(y_axis_label, fontsize=14)

    if df_census[y_field].max() > 500:
        ax.yaxis.set_major_formatter(DollarFormatter())

        for tickloc, ticklabel in zip(ax.get_yticks(), ax.get_yticklabels()):
            if tickloc < 0:
                ticklabel.set_color("red")

    ax.set_axisbelow(True)

    ax.grid()
    ax.axhline(color='black', )

    if plot_ols or plot_log_ols or plot_wls:
        ax.legend()

    subtitle = []

    subtitle.append("n = {:d}".format(df_census.shape[0]))

    if plot_ols:
        subtitle.append("m/decile = {:,.0f}, r^2 = {:0.3f}".format(
            res_ols.params[0] / 10, res_ols.rsquared
        ))

    if plot_log_ols:
        subtitle.append("log r^2 = {:0.3f}".format(
            res_log_ols.rsquared
        ))

    if plot_wls:
        subtitle.append("m_w/decile = {:,.0f}, r^2_w = {:0.3f}".format(
            res_ols.params[0] / 10, res_wls.rsquared
        ))

    if len(subtitle) > 0:
        title = title + "\n(" + '; '.join(subtitle) + ')'

    ax.set_title(title)

    plt.show()

    if plot_ols or plot_wls:
        rsquared = res_ols.rsquared if plot_ols else res_wls.rsquared
        return df_y - fittedvalues, rsquared

    return None, None


def _pop_string(min_pop, max_pop):
    if min_pop is not None:
        if max_pop is not None:
            pop_string = "{:,d}-{:,d}".format(min_pop, max_pop)
        else:
            pop_string = "over {:,d}".format(min_pop)
    else:
        if max_pop is not None:
            pop_string = "up to {:,d}".format(max_pop)
        else:
            pop_string = "any"

    return pop_string


def scatter_cousub(
        df_census,
        y_field,
        x_field = 'diversity',
        y_label = None,
        n_largest = None,
        min_pop = None,
        max_pop = None,
        plot_ols = False,
        plot_wls = False,
        plot_log_ols = False,
        size_population = False,
        regression_intercept = True,
):
    if y_label is None:
        y_label = y_field

    pop_string = _pop_string(min_pop, max_pop)

    title = "{} vs. {}\nCommunity Population {:s}".format(
        y_label, x_field.capitalize(),
        pop_string)

    _scatter_plot(
        df_census,
        x_field,
        y_field,
        title,
        [y_label],
        plot_ols=plot_ols,
        plot_wls=plot_wls,
        plot_log_ols=plot_log_ols,
        size_population=size_population,
        regression_intercept=regression_intercept,
    )

    return df_census


def diff_field_name(field0, field1):
    return '-'.join(['m', field0, field1])


def not_field_name(field):
    return field + '-prime'


def scatter_cousub_delta(
        df_census,
        census_fields,
        x_field = 'diversity',
        labels = None,
        n_largest = None,
        min_pop = None,
        max_pop = None,
        plot_ols = False,
        plot_wls = False,
        size_population = False,
        highlight_names = None,
        regression_intercept = False,
):
    if labels is None:
        labels = census_fields

    diff_field = diff_field_name(census_fields[0], census_fields[1])

    df_census[diff_field] = df_census[census_fields[0]] - df_census[census_fields[1]]

    pop_string = _pop_string(min_pop, max_pop)

    title = "Difference between {} and {} vs. {}\nCommunity Population {:s}".format(
        labels[0], labels[1], x_field.capitalize(),
        pop_string)

    prstd, rsquared = _scatter_plot(
        df_census,
        x_field,
        diff_field,
        title,
        labels,
        plot_ols=plot_ols,
        plot_wls=plot_wls,
        size_population=size_population,
        highlight_names=highlight_names,
        regression_intercept=regression_intercept,
    )

    return df_census, prstd, rsquared


def scatter_cousub_frac_delta(
        df_census,
        census_fields,
        x_field = 'diversity',
        labels = None,
        n_largest = None,
        min_pop = None,
        max_pop = None,
        plot_ols = False,
        plot_wls = False,
        plot_log_ols = False,
        size_population = False,
        highlight_names = None,
        regression_intercept = True,
):
    if labels is None:
        labels = census_fields

    diff_field = diff_field_name(census_fields[0], census_fields[1])

    df_census[diff_field] = df_census[census_fields[0]] / df_census[census_fields[1]]

    pop_string = _pop_string(min_pop, max_pop)

    title = "Change from {} to {} vs. {}\nCommunity Population {:s}".format(
        labels[0], labels[1], x_field.capitalize(),
        pop_string)

    _scatter_plot(
        df_census,
        x_field,
        diff_field,
        title,
        labels,
        plot_ols=plot_ols,
        plot_wls=plot_wls,
        plot_log_ols=plot_log_ols,
        size_population=size_population,
        highlight_names=highlight_names,
        regression_intercept=regression_intercept,
        y_axis_label = labels[0] + ' / ' + labels[1],
    )

    return df_census


def plot_diversity_scatter(
        df_census,
        x_field = 'diversity',
        y_field = 'segregation',
        highlight_names = None,
        title = None,
):
    ax = df_census.plot(x_field, y_field, kind='scatter')

    if highlight_names is not None:
        df_highlight = df_census[df_census.NAME.isin(highlight_names)]
        _plot_highlights(df_highlight, x_field, y_field, ax)

    ax.set_xbound(lower=0)
    ax.set_ybound(lower=0)
    ax.set_xlabel(x_field.capitalize())
    ax.set_ylabel(y_field.capitalize())
    ax.grid()

    if title is None:
        title = y_field.capitalize() + ' vs. ' + x_field.capitalize()

    title = title + '\n(n = {:.0f})'.format(df_census.shape[0])

    ax.set_title(title)