import censusdata
import numpy as np
import pandas as pd

import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
from statsmodels.stats.weightstats import DescrStatsW

from usdis.read import read_usdi_counties, read_usdi_cousubs

DEFAULT_YEAR = 2019

FIELD_TOTAL_POPULATION = 'B01001_001E'

FIELD_MEDIAN_HOUSEHOLD_INCOME = 'B19013_001E'
FIELD_MEDIAN_HOUSEHOLD_INCOME_BLACK = 'B19013B_001E'
FIELD_MEDIAN_HOUSEHOLD_INCOME_WHITE_NOT_HISPANIC = 'B19013H_001E'

FIELD_PER_CAPITA_INCOME = 'B19301_001E'
FIELD_PER_CAPITA_INCOME_WHITE_NOT_HISPANIC = 'B19301H_001E'


def read_key(filepath):
    """
    Read a key from a file. We will read the first line of the
    file and return a string.
    :param filepath: The path to the file.
    :return: The first line of the file as a string.
    """
    with open(filepath) as file:
        for line in file:
            return line.strip()

    return None


def per_capita_non_group(
    per_capita_group,
    population_group,
    per_capita_all,
    population_all,
):
    """
    Compute the per-capita amount of some value among
    members of a population who are not in a group when
    you know it for members of the group and the total
    population.
    :param per_capita_group:
    :param population_group:
    :param per_capita_all:
    :param population_all:
    :return:
    """
    population_non_group = population_all - population_group

    total_group = per_capita_group * population_group
    total_all = per_capita_all * population_all
    total_non_group = total_all - total_group

    per_capita_non_group = total_non_group / population_non_group

    return per_capita_non_group


class UsdisCensus:
    def __init__(self, census_api_key, year=DEFAULT_YEAR):
        self._census_api_key = census_api_key
        self._year = year

    @staticmethod
    def geo_to_state(geo):
        d = dict(geo.params())
        return d['state']

    @staticmethod
    def geo_to_county(geo):
        d = dict(geo.params())
        return d['county']

    @staticmethod
    def geo_to_cousub(geo):
        d = dict(geo.params())
        return d['county subdivision']


    def usdi_county_censusdata(self, states, counties, census_fields):
        df_census = censusdata.download(
            'acs5',
            self._year,
            censusdata.censusgeo([
                ('state', states),
                ('county', counties)]),
            census_fields,
            key=self._census_api_key,
        )

        df_census['STATEFP'] = df_census.index.map(self.geo_to_state)
        df_census['COUNTYFP'] = df_census.index.map(self.geo_to_county)

        df_census = df_census[['STATEFP', 'COUNTYFP'] + census_fields]
        df_census.reset_index(inplace=True, drop=True)

        df_usdi_counties = read_usdi_counties(self._year)

        df = df_census.merge(df_usdi_counties, left_on=['STATEFP', 'COUNTYFP'], right_on=['STATEFP', 'COUNTYFP'])

        non_census_fields = [field for field in df.columns if field not in census_fields]

        df = df[non_census_fields + census_fields]

        return df


    def usdi_cousub_censusdata(self, states, cousubs, census_fields, deltas=False):
        df_census = censusdata.download(
            'acs5',
            self._year,
            censusdata.censusgeo([
                ('state', states),
                # ('county', '*'),
                ('county subdivision', cousubs)]),
            census_fields,
            key=self._census_api_key,
        )

        df_census['STATEFP'] = df_census.index.map(self.geo_to_state)
        df_census['COUNTYFP'] = df_census.index.map(self.geo_to_county)
        df_census['COUSUBFP'] = df_census.index.map(self.geo_to_cousub)

        df_census = df_census[['STATEFP', 'COUNTYFP', 'COUSUBFP'] + census_fields]
        df_census.reset_index(inplace=True, drop=True)

        df_usdi_cousubs = read_usdi_cousubs(self._year)

        df = df_census.merge(df_usdi_cousubs, on=['STATEFP', 'COUNTYFP', 'COUSUBFP'])

        non_census_fields = [field for field in df.columns if field not in census_fields]

        df = df[non_census_fields + census_fields]

        # A very small number sneak through for some ill-defined
        # places.
        df = df.dropna()

        if deltas:
            for ii, f0 in enumerate(census_fields):
                if f0 != FIELD_TOTAL_POPULATION:
                    for f1 in census_fields[ii + 1:]:
                        if f1 != FIELD_TOTAL_POPULATION:
                            df['-'.join(['d', f1, f0])] = df[f1] - df[f0]

        return df


    def usdi_cosub_bounded_censusdata(
            self,
            states,
            cousubs,
            census_fields,
            n_largest = None,
            min_pop = None,
            max_pop = None,
    ):
        all_fields = [FIELD_TOTAL_POPULATION] + census_fields

        df_census = self.usdi_cousub_censusdata(states, cousubs, all_fields)
        for census_field in census_fields:
            df_census = df_census[df_census[census_field]!=-666666666]

        if min_pop is not None:
            df_census = df_census[df_census[FIELD_TOTAL_POPULATION]>=min_pop]
        if max_pop is not None:
            df_census = df_census[df_census[FIELD_TOTAL_POPULATION]<max_pop]
        if n_largest is not None:
            df_census = df_census.sort_values(by=FIELD_TOTAL_POPULATION, ascending=False).head(n_largest)

        df_census = pd.DataFrame(df_census)

        return df_census
